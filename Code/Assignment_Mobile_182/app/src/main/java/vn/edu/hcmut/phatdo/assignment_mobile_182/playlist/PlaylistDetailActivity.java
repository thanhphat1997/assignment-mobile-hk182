package vn.edu.hcmut.phatdo.assignment_mobile_182.playlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import butterknife.BindView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.PlaylistModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.music.MediaPlayerService;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.music.PlayMusicActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.music.SongListAdapter;
import vn.edu.hcmut.phatdo.assignment_mobile_182.utils.SongUtils;

public class PlaylistDetailActivity extends BaseActivity implements SongListAdapter.SongListener {

    private final int REQUEST_CODE_ADD_SONG = 1111;

    private List<SongModel> mSongList = new ArrayList<>();

    List<SongModel> mSelected = new ArrayList<>();

    SelectionTracker<Long> mTracker;

    SongListAdapter mAdapter;

    SQLiteDBHelper mDBHelper;

    private PlaylistModel mPlaylist;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Override
    protected int getResLayoutId() {
        return R.layout.layout_playlist_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_playlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_add:
                Intent intent = new Intent(this, AddSongActivity.class);
                intent.putParcelableArrayListExtra("songList", (ArrayList<? extends Parcelable>) mSongList);
                startActivityForResult(intent, REQUEST_CODE_ADD_SONG);
                return true;
            case R.id.item_delete:
                Selection selection = mTracker.getSelection();
                Iterator i = selection.iterator();
                while (i.hasNext()) {
                    long index = (long) i.next();
                    mSelected.add(mSongList.get((int) index));
                }
                for (SongModel song : mSelected) {
                    mSongList.remove(song);
                    mAdapter.notifyDataSetChanged();
                    updateTitle();
                }
                mDBHelper.removePlaylistSong(mSelected, mPlaylist.id);
                mTracker.clearSelection();
                return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != REQUEST_CODE_ADD_SONG || resultCode != Activity.RESULT_OK || data == null) {
            return;
        }

        List<SongModel> selected = data.getParcelableArrayListExtra("result");
        if (selected == null || selected.isEmpty()) {
            return;
        }

        mSongList.addAll(selected);
        mDBHelper.insertSongToPlaylist(selected, mPlaylist.id);
        SongUtils.sort(mSongList);
        updateTitle();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPlaylist = getIntent().getExtras().getParcelable("playlist");
        mDBHelper = new SQLiteDBHelper(this);
        getSongList();
        updateTitle();
        mAdapter = new SongListAdapter(getApplicationContext(), mSongList, true);
        mAdapter.setListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setDataList(mSongList);
        mTracker = new SelectionTracker.Builder<>(
                "mySelection",
                mRecyclerView,
                new StableIdKeyProvider(mRecyclerView),
                new MyItemDetailsLookup(mRecyclerView),
                StorageStrategy.createLongStorage()
        ).withSelectionPredicate(
                SelectionPredicates.<Long>createSelectAnything()
        ).build();
        mAdapter.setTracker(mTracker);
    }

    private void getSongList() {
        List<Long> idList = mDBHelper.getAllPlaylistSong(mPlaylist.id);
        mSongList = SongUtils.getSongList(idList);
    }

    private void updateTitle() {
        String title = mPlaylist.name + " (" + mSongList.size() + " songs)";
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onClickItem(int position) {
        Intent intent = new Intent(this, PlayMusicActivity.class);
        intent.setAction("playlist");
        Bundle bundle = new Bundle();
        bundle.putParcelable("playlist", mPlaylist);
        intent.putExtras(bundle);
        intent.putParcelableArrayListExtra("songList", (ArrayList<? extends Parcelable>) mSongList);
        startActivity(intent);
        finish();
    }
}
