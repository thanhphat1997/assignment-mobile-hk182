package vn.edu.hcmut.phatdo.assignment_mobile_182.news;

import java.util.Date;

public class News {
    public String singerName;
    public Date date;
    public String text;
    public String profileImageURL;
    public String previewImageURL;
    public String watchURL;

    public News(String singerName, Date date, String text, String profileImageURL, String previewImageURL, String watchURL) {
        this.singerName = singerName;
        this.date = date;
        this.text = text;
        this.profileImageURL = profileImageURL;
        this.previewImageURL = previewImageURL;
        this.watchURL = watchURL;
    }

}
