package vn.edu.hcmut.phatdo.assignment_mobile_182.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;

public class SQLiteDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "local_music";
    public static final String PLAYLIST_TABLE_NAME = "playlist";
    public static final String PLAYLIST_COLUMN_ID = "playlist_id";
    public static final String PLAYLIST_COLUMN_NAME = "name";
    public static final String SONG_TABLE_NAME = "song";
    public static final String SONG_COLUMN_ID = "song_id";
    public static final String SONG_COLUMN_LISTEN = "listen";
    public static final String SONG_COLUMN_TIME = "timestamp";
    public static final String SONG_COLUMN_LOVE = "love";
    public static final String PLAYLIST_SONG_TABLE_NAME = "playlist_song";

    public SQLiteDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + PLAYLIST_TABLE_NAME + " (" +
                PLAYLIST_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PLAYLIST_COLUMN_NAME + " TEXT" + ")");
        db.execSQL("CREATE TABLE " + SONG_TABLE_NAME + " (" +
                SONG_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                SONG_COLUMN_TIME + " INTEGER , " +
                SONG_COLUMN_LISTEN + " INTEGER , " +
                SONG_COLUMN_LOVE + " INTEGER" + ")");
        db.execSQL("CREATE TABLE " + PLAYLIST_SONG_TABLE_NAME + " (" +
                PLAYLIST_COLUMN_ID + " INTEGER , " +
                SONG_COLUMN_ID + " INTEGER, " +
                " PRIMARY KEY ( " + PLAYLIST_COLUMN_ID + ", " + SONG_COLUMN_ID + "))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SONG_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_SONG_TABLE_NAME);
        onCreate(db);
    }

    public void insertPlaylist(String playlist) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLiteDBHelper.PLAYLIST_COLUMN_NAME, playlist);
        database.insert(SQLiteDBHelper.PLAYLIST_TABLE_NAME, null, values);
        database.close();
    }

    public List<PlaylistModel> readAllPlaylist() {
        SQLiteDatabase database = this.getReadableDatabase();
        List<PlaylistModel> list = new ArrayList<>();

        database.execSQL("CREATE TABLE IF NOT EXISTS " + PLAYLIST_TABLE_NAME + " (" +
                PLAYLIST_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PLAYLIST_COLUMN_NAME + " TEXT" + ")");

        String[] projection = {
                SQLiteDBHelper.PLAYLIST_COLUMN_ID,
                SQLiteDBHelper.PLAYLIST_COLUMN_NAME,
        };

        Cursor cursor = database.query(
                SQLiteDBHelper.PLAYLIST_TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort
        );

        if (cursor.moveToFirst()) {
            do {
                list.add(new PlaylistModel(cursor.getLong(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        Collections.sort(list, new Comparator<PlaylistModel>() {
            @Override
            public int compare(PlaylistModel lhs, PlaylistModel rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        database.close();
        return list;
    }

    public void removePlaylist(List<PlaylistModel> list) {
        SQLiteDatabase database = this.getWritableDatabase();
        for (PlaylistModel playlist: list) {
            database.delete(PLAYLIST_TABLE_NAME, "playlistId=?", new String[]{Long.toString(playlist.id)});
        }
        database.close();
    }

    public void insertSongToPlaylist(List<SongModel> list, long playlistId) {
        SQLiteDatabase database = this.getWritableDatabase();
        for (SongModel song: list) {
            ContentValues values = new ContentValues();
            values.put(SONG_COLUMN_ID, song.getId());
            values.put(PLAYLIST_COLUMN_ID, playlistId);
            database.insert(PLAYLIST_SONG_TABLE_NAME, null, values);
        }
        database.close();
    }

    public void insertSong(List<SongModel> list) {
        SQLiteDatabase database = this.getWritableDatabase();
        for (SongModel song: list) {
            ContentValues values = new ContentValues();
            values.put(SONG_COLUMN_ID, song.getId());
            values.put(SONG_COLUMN_LISTEN, 0);
            values.put(SONG_COLUMN_TIME, System.currentTimeMillis());
            values.put(SONG_COLUMN_LOVE, 0);
            database.insert(SONG_TABLE_NAME, null, values);
        }
        database.close();
    }

    public void removePlaylistSong(List<SongModel> list, long playlistId) {
        SQLiteDatabase database = this.getWritableDatabase();
        for (SongModel song: list) {
            database.delete(PLAYLIST_SONG_TABLE_NAME, "song_id=? and playlist_id=?", new String[]{Long.toString(song.getId()), Long.toString(playlistId)});
        }
        database.close();
    }

    public List<Long> getAllPlaylistSong(long playlistId) {
        SQLiteDatabase database = this.getReadableDatabase();
        List<Long> list = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM " + PLAYLIST_SONG_TABLE_NAME + " WHERE playlist_id = " + playlistId, null);

        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getLong(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        Collections.sort(list);
        database.close();
        return list;
    }

    public List<Long> getAllLoveSong() {
        SQLiteDatabase database = this.getReadableDatabase();
        List<Long> list = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM " + SONG_TABLE_NAME + " WHERE love = " + 1, null);

        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getLong(0));
            } while (cursor.moveToNext());
        }
        cursor.close();
        Collections.sort(list);
        database.close();
        return list;
    }

    public void insertLoveSong(SongModel song) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SONG_COLUMN_LOVE, 1);
        database.update(SONG_TABLE_NAME, values, "song_id=?", new String[]{Long.toString(song.getId())});
        database.close();
    }

    public void removeLoveSong(SongModel song) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SONG_COLUMN_LOVE, 0);
        database.update(SONG_TABLE_NAME, values, "song_id=?", new String[]{Long.toString(song.getId())});
        database.close();
    }

    public List<SongDBModel> getAllSong() {
        SQLiteDatabase database = this.getReadableDatabase();
        List<SongDBModel> list = new ArrayList<>();
        String[] projection = {
                SQLiteDBHelper.SONG_COLUMN_ID,
                SQLiteDBHelper.SONG_COLUMN_LISTEN,
                SONG_COLUMN_TIME,
        };

        Cursor cursor = database.query(
                SONG_TABLE_NAME,   // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // don't sort
        );

        if (cursor.moveToFirst()) {
            do {
                list.add(new SongDBModel(cursor.getLong(0), cursor.getLong(2), cursor.getLong(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();
        return list;
    }

    public void updateSong(SongModel song) {
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + SONG_TABLE_NAME + " WHERE song_id = " + song.getId(), null);
        ContentValues values = new ContentValues();
        values.put(SONG_COLUMN_TIME, System.currentTimeMillis());
        if (cursor.moveToFirst()) {
            long listen = cursor.getLong(3);
            values.put(SONG_COLUMN_LISTEN, listen + 1);
            database.update(SONG_TABLE_NAME, values, "song_id=?", new String[]{Long.toString(song.getId())});
            cursor.close();
            database.close();
            return;
        }
        cursor.close();
        values.put(SONG_COLUMN_TIME, 1);
        database.insert(SONG_TABLE_NAME, null, values);
        database.close();
    }
}
