package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.utils.SongUtils;

public class MediaPlayerService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    public static final String ACTION_PLAY = "action_play";
    public static final String ACTION_PAUSE = "action_pause";

    private MyMediaPlayer mMediaPlayer = MyMediaPlayer.getInstance();
    private int mResumePosition;
    private List<SongModel> mSongList = new ArrayList<>();
    SQLiteDBHelper dbHelper;
    private static String mAction = "";
    private static int mCurrentSong = 0;
    Timer timer;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            if (TextUtils.isEmpty(action)) {
                return;
            }

            switch (action) {
                case "play":
                    if (mMediaPlayer.isPlaying()) {
                        pauseMedia();
                    } else {
                        if (mMediaPlayer.getCurrentPosition() > 0) {
                            resumeMedia();
                        } else {
                            playMedia();
                        }
                    }
                    break;
                case "next":
                    gotoNext();
                    break;
                case "previous":
                    gotoPrevious();
                    break;
                case "progress":
                    mResumePosition = intent.getIntExtra("progress", 0);
                    if (mResumePosition <= 0) {
                        return;
                    }

                    if (mMediaPlayer.isPlaying()) {
                        mMediaPlayer.pause();
                    }
                    resumeMedia();
                    break;
                case "status":
                    sendDuration();
                    sendProgress();
                    sendStatus();
                    sendDetail();
                    break;
                case "goto":
                    int position = intent.getIntExtra("position", mCurrentSong);
                    gotoPosition(position);
                    break;
                case "stoptimer":
                    stopTimer();
                    break;
                case "starttimer":
                    startTimer();
                    break;
                default:
                    break;
            }
        }
    };

    private void gotoPosition(int position) {
        try {
            if (position >= mSongList.size() || position < 0) {
                return;
            }

            stopMedia();
            mCurrentSong = position;
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mSongList.get(mCurrentSong).getPath());
            prepareSong();
            playMedia();
            mMediaPlayer.seekTo(0);
        } catch (IOException e) {
            e.printStackTrace();
            stopSelf();
        }
    }

    private void gotoNext() {
        try {
            stopMedia();
            mCurrentSong = (mCurrentSong + 1) % mSongList.size();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mSongList.get(mCurrentSong).getPath());
            prepareSong();
            playMedia();
            mMediaPlayer.seekTo(0);
        } catch (IOException e) {
            e.printStackTrace();
            stopSelf();
        }
    }

    private void gotoPrevious() {
        try {
            stopMedia();
            mCurrentSong = (mCurrentSong - 1 + mSongList.size()) % mSongList.size();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mSongList.get(mCurrentSong).getPath());
            prepareSong();
            playMedia();
            mMediaPlayer.seekTo(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            stopMedia();
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
        unregisterReceiver(mReceiver);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter iF = new IntentFilter();
        iF.addAction("com.android.music.command");
        registerReceiver(mReceiver, iF);
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
//        Intent intent = new Intent();
//        intent.setAction("com.android.music.complete");
//        sendBroadcast(intent);
        dbHelper.updateSong(mSongList.get(mCurrentSong));
        gotoNext();
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        //Invoked when there has been an error during an asynchronous operation
        switch (i) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Log.d("MediaPlayer Error", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + i1);
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.d("MediaPlayer Error", "MEDIA ERROR SERVER DIED " + i1);
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.d("MediaPlayer Error", "MEDIA ERROR UNKNOWN " + i1);
                break;
        }
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getStringExtra("action");
        if (action.equalsIgnoreCase(ACTION_PLAY)) {
            startForegroundService();
        } else if (action.equalsIgnoreCase(ACTION_PAUSE)) {
            stopForegroundService();
            return super.onStartCommand(intent, flags, startId);
        }

        dbHelper = new SQLiteDBHelper(getApplicationContext());
        if (mSongList == null || mSongList.isEmpty() || !mAction.equalsIgnoreCase(intent.getAction()) || !intent.getAction().equalsIgnoreCase("")) {
            mCurrentSong = 0;
            updateDataSource(intent);
            initMediaPlayer();
        }

        startTimer();
        return super.onStartCommand(intent, flags, startId);
    }

    private void initMediaPlayer() {
        //Set up MediaPlayer event listeners
        mMediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnErrorListener(this);
        //Reset so that the MediaPlayer is not pointing to another data source
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        prepareSong();
    }

    private void prepareSong() {
        if (mSongList == null || mSongList.isEmpty()) {
            return;
        }

        try {
            mMediaPlayer.prepare();
            sendDuration();
            sendProgress();
            sendDetail();
        } catch (IOException e) {
            e.printStackTrace();
            stopSelf();
        }
    }

    private void updateDataSource(Intent intent) {
        mAction = intent.getAction();
        if (mAction.equalsIgnoreCase("playlist")) {
            mSongList = intent.getParcelableArrayListExtra("songList");
        } else if (mAction.equalsIgnoreCase("lovesong")) {
            mSongList = SongUtils.getSongList(dbHelper.getAllLoveSong());
        } else {
            mSongList = SongUtils.mSongList;
        }
        mMediaPlayer.reset();
        if (mSongList == null || mSongList.isEmpty()) {
            return;
        }

        try {
            mMediaPlayer.setDataSource(mSongList.get(0).getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendDuration() {
        int duration = mMediaPlayer.getDuration();
        Intent intent = new Intent();
        intent.setAction("com.android.music.duration");
        intent.putExtra("duration", duration);
        sendBroadcast(intent);
    }

    private void sendProgress() {

    }

    private void stopTimer() {
        timer.cancel();
        timer = null;
    }

    private void startTimer() {
        if (timer == null) {
            timer = new Timer();
            TimerTask doAsynchronousTask = new TimerTask() {
                @Override
                public void run() {
                    if (mMediaPlayer.isPlaying()) {
                        Intent intent = new Intent();
                        intent.setAction("com.android.music.updateprogress");
                        intent.putExtra("progress", mMediaPlayer.getCurrentPosition());
                        sendBroadcast(intent);
                    }
                }
            };
            timer.schedule(doAsynchronousTask, 0, 1000);
        }
    }

    private void sendDetail() {
        Intent intent = new Intent();
        intent.setAction("com.android.music.song");
        intent.putExtra("song", mCurrentSong);
        sendBroadcast(intent);
    }

    private void sendStatus() {
        boolean status = mMediaPlayer.isPlaying();
        Intent intent = new Intent();
        intent.setAction("com.android.music.updatestatus");
        intent.putExtra("status", status);
        sendBroadcast(intent);
    }

    private void playMedia() {
        if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer.start();
            sendStatus();
        }
    }

    private void stopMedia() {
        if (mMediaPlayer == null) return;
        if (mMediaPlayer.isPlaying()) {
            mResumePosition = 0;
            mMediaPlayer.stop();
            sendStatus();
        }
    }

    private void pauseMedia() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            mResumePosition = mMediaPlayer.getCurrentPosition();
            sendStatus();
        }
    }

    private void resumeMedia() {
        if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer.seekTo(mResumePosition);
            mMediaPlayer.start();
            sendStatus();
        }
    }

    private void startForegroundService() {
        // Create notification default intent.
        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Create notification builder.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        // Make notification show big text.
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle("Music player implemented by foreground service.");
        bigTextStyle.bigText("Android foreground service is a android service which can run in foreground always, it can be controlled by user via notification.");
        // Set big text style.
        builder.setStyle(bigTextStyle);

        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher);
        Bitmap largeIconBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_local_music);
        builder.setLargeIcon(largeIconBitmap);
        // Make the notification max priority.
        builder.setPriority(Notification.PRIORITY_MAX);
        // Make head-up notification.
        builder.setFullScreenIntent(pendingIntent, true);

        // Add Play button intent in notification.
        Intent playIntent = new Intent(this, MediaPlayerService.class);
        playIntent.setAction(ACTION_PLAY);
        PendingIntent pendingPlayIntent = PendingIntent.getService(this, 0, playIntent, 0);
        NotificationCompat.Action playAction = new NotificationCompat.Action(android.R.drawable.ic_media_play, "Play", pendingPlayIntent);
        builder.addAction(playAction);

        // Add Pause button intent in notification.
        Intent pauseIntent = new Intent(this, MediaPlayerService.class);
        pauseIntent.setAction(ACTION_PAUSE);
        PendingIntent pendingPrevIntent = PendingIntent.getService(this, 0, pauseIntent, 0);
        NotificationCompat.Action prevAction = new NotificationCompat.Action(android.R.drawable.ic_media_pause, "Pause", pendingPrevIntent);
        builder.addAction(prevAction);

        // Build the notification.
        Notification notification = builder.build();

        // Start foreground service.
        startForeground(1, notification);
    }

    private void stopForegroundService() {
        // Stop foreground service and remove the notification.
        stopForeground(true);

        // Stop the foreground service.
        stopSelf();
        MyMediaPlayer.release = true;
    }
}
