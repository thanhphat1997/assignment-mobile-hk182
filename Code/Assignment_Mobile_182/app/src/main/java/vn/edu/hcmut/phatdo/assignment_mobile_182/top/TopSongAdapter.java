package vn.edu.hcmut.phatdo.assignment_mobile_182.top;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.video.VideoRecyclerViewAdapter;
import vn.edu.hcmut.phatdo.assignment_mobile_182.shared.CircleTransform;


// Note that we specify the custom ViewHolder which gives us access to our views
public class TopSongAdapter extends
        RecyclerView.Adapter<TopSongAdapter.ViewHolder> {

    // Store a member variable for the contacts
    private List<MySong> items ;
    VideoRecyclerViewAdapter.CustomItemClickListener listener;

    // Pass in the contact array into the constructor
    public TopSongAdapter(List<MySong> list, VideoRecyclerViewAdapter.CustomItemClickListener listener) {
        items = list;
        this.listener = listener;
    }

    public void addMore(List<MySong> newItems) {
        int last = items.size();
        items.addAll(newItems);
        notifyItemRangeInserted(last,newItems.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_top_song, parent, false);

        // Return a new holder instance
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, viewHolder.getPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        // Get the data model based on position
        final MySong item = items.get(position);

        // Set item views based on your views and data model
        holder.singerNameTextView.setText(item.artist);
        holder.songNameTextView.setText(item.title);
        Picasso.get().load(item.imgSrc).transform(new CircleTransform()).into(holder.profileImageView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public ImageView profileImageView;
        public TextView singerNameTextView;
        public TextView songNameTextView;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            final View view = itemView;

            profileImageView = view.findViewById(R.id.topsong_profile_image);
            singerNameTextView =  view.findViewById(R.id.topsong_singer_name);
            songNameTextView =  view.findViewById(R.id.topsong_song_name);
        }
    }



}

