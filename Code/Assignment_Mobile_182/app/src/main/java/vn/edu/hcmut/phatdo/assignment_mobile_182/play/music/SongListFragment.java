package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseFragment;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.PlaylistModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.utils.SongUtils;

public class SongListFragment extends BaseFragment implements SongListAdapter.SongListener {

    @BindView(R.id.song_list)
    RecyclerView mRecyclerView;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    private List<SongModel> mSongList;
    private SongListAdapter mAdapter;
    private SQLiteDBHelper mDBHelper;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDBHelper = new SQLiteDBHelper(getContext());
        String action = getActivity().getIntent().getAction();
        if (action == null) {
            action = "";
        }
        mAdapter = new SongListAdapter(mSongList);
        mAdapter.setListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        updateSongList(action);
    }

    private void updateSongList(String action) {
        String name;
        if (action.equalsIgnoreCase("playlist")) {
            PlaylistModel playlist = getActivity().getIntent().getExtras().getParcelable("playlist");
            name = playlist.name;
            mSongList = getActivity().getIntent().getParcelableArrayListExtra("songList");
        } else if (action.equalsIgnoreCase("lovesong")) {
            name = "LOVE SONGS";
            mSongList = SongUtils.getSongList(mDBHelper.getAllLoveSong());
        } else {
            name = "ALL SONGS";
            mSongList = SongUtils.mSongList;
        }
        String title = name + " (" + mSongList.size() + ")";
        tvTitle.setText(title);
        mAdapter.setDataList(mSongList);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        mAdapter.setListener(null);
        super.onDestroyView();
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.layout_song_list;
    }

    @Override
    public void onClickItem(int position) {
        playSong(position);
        changePage();
    }

    private void changePage() {
        Intent intent = new Intent();
        intent.setAction("com.android.music.changepage");
        intent.putExtra("page", PlayMusicPage.PLAY);
        getActivity().sendBroadcast(intent);
    }

    private void playSong(int position) {
        Intent intent = new Intent();
        intent.setAction("com.android.music.command");
        intent.putExtra("action", "goto");
        intent.putExtra("position", position);
        getActivity().sendBroadcast(intent);
    }
}
