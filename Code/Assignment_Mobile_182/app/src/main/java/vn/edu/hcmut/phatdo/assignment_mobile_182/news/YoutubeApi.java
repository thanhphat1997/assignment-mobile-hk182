package vn.edu.hcmut.phatdo.assignment_mobile_182.news;

/**
 * Sample Java code for youtube.videos.list
 * See instructions for running these code samples locally:
 * https://developers.google.com/explorer-help/guides/code_samples#java
 */

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public class YoutubeApi {
    // You need to set this value for your code to compile.
    // For example: ... DEVELOPER_KEY = "YOUR ACTUAL KEY";
    private static final String DEVELOPER_KEY = "AIzaSyATR1wUJwr6RZi7-dAPvqwTe2WMe1fNgVs";

    private static final String APPLICATION_NAME = "API code samples";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String CATEGORY_MUSIC = "10";
    private static final String REGION_CODE = "VN";

    /**
     * Build and return an authorized API client service.
     *
     * @return an authorized API client service
     * @throws GeneralSecurityException, IOException
     */
    public static YouTube getService() throws GeneralSecurityException, IOException {
        final NetHttpTransport httpTransport = new com.google.api.client.http.javanet.NetHttpTransport();
        return new YouTube.Builder(httpTransport, JSON_FACTORY, null)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    /**
     * Call function to create API service object. Define and
     * execute API request. Print API response.
     *
     * @throws GeneralSecurityException, IOException, GoogleJsonResponseException
     */
    public static VideoResponse getVideoResponse(String pageToken)
            throws GeneralSecurityException, IOException, GoogleJsonResponseException {
        YouTube youtubeService = getService();
        // Define and execute the API request
        YouTube.Videos.List videoRequest = youtubeService.videos()
                .list("snippet");
        YouTube.Channels.List channelRequest = youtubeService.channels()
                .list("snippet");
        VideoListResponse videoListResponse = videoRequest.setKey(DEVELOPER_KEY)
                .setChart("mostPopular")
                .setPageToken(pageToken)
                .setRegionCode(REGION_CODE)
                .setVideoCategoryId(CATEGORY_MUSIC)
                .execute();

        List<VideoData> videoDataList = new ArrayList<>();
        String nextPageToken = videoListResponse.getNextPageToken();
        for(Video video : videoListResponse.getItems()){
            VideoData videoData = new VideoData(video);
            videoDataList.add(videoData);
            //get channel info
            ChannelListResponse channelListResponse = channelRequest.setKey(DEVELOPER_KEY)
                    .setId(videoData.getChannelId())
                    .execute();
            String channelImgURL = channelListResponse.getItems().get(0).getSnippet().getThumbnails().getDefault().getUrl();
            videoData.setChannelImageURL(channelImgURL);
        }
        return new VideoResponse(videoDataList, nextPageToken);
    }
}
