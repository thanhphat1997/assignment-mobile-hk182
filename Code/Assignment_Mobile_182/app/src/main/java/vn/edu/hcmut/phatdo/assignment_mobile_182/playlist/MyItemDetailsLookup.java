package vn.edu.hcmut.phatdo.assignment_mobile_182.playlist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import androidx.recyclerview.selection.ItemDetailsLookup;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.AbsViewHolder;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.music.SongListAdapter;

public class MyItemDetailsLookup extends ItemDetailsLookup<Long> {
    private RecyclerView mRecyclerView;

    public MyItemDetailsLookup(RecyclerView recyclerView) {
        this.mRecyclerView = recyclerView;
    }

    @Override
    public ItemDetails<Long> getItemDetails(@NonNull MotionEvent e) {
        View view = mRecyclerView.findChildViewUnder(e.getX(), e.getY());

        if (view != null) {
            return ((AbsViewHolder) mRecyclerView.getChildViewHolder(view)).getItemDetails();
        }
        return null;
    }
}
