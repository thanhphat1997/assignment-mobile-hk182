package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.AbsViewHolder;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;


public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.MusicViewHolder> {

    private List<SongModel> mDataList;

    private SongListener mListener;

    private SelectionTracker mTracker;

    private Context mContext;

    public SongListAdapter(List<SongModel> dataList) {
        this.mDataList = dataList;
    }

    public SongListAdapter(Context context, List<SongModel> dataList, boolean hasStableId) {
        this.mContext = context;
        this.mDataList = dataList;
        setHasStableIds(hasStableId);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @NonNull
    @Override
    public MusicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.song_item_full_width, parent, false);
        return new MusicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MusicViewHolder holder, final int position) {
        if (mTracker != null) {
            if (mTracker.isSelected((long) position)) {
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.selected));
            } else {
                holder.itemView.setBackgroundColor(Color.WHITE);
            }
        }
        holder.tvName.setText(mDataList.get(position).getName());
        holder.tvSinger.setText(mDataList.get(position).getSinger());
        holder.icSong.setImageURI(mDataList.get(position).getIcon());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener == null) {
                    return;
                }
                mListener.onClickItem(position);
            }
        });
    }

    public void setDataList(List<SongModel> songList) {
        mDataList = songList;
        notifyDataSetChanged();
    }

    public class MusicViewHolder extends AbsViewHolder {
        View mView;
        TextView tvSinger;
        TextView tvName;
        ImageView icSong;

        MusicViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            tvName = mView.findViewById(R.id.tv_name);
            tvSinger = mView.findViewById(R.id.tv_singer);
            icSong = mView.findViewById(R.id.ic_song);
        }

        @Override
        public ItemDetailsLookup.ItemDetails<Long> getItemDetails() {
            return new ItemDetailsLookup.ItemDetails<Long>() {
                @Override
                public int getPosition() {
                    return getAdapterPosition();
                }

                @Override
                public Long getSelectionKey() {
                    return getItemId();
                }
            };
        }
    }

    public void setListener(SongListener listener) {
        mListener = listener;
    }

    public void setTracker(SelectionTracker tracker) {
        this.mTracker = tracker;
    }

    public interface SongListener {
        void onClickItem(int position);
    }
}
