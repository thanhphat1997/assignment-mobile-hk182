package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import butterknife.BindView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseActivity;

public class PlayMusicActivity extends BaseActivity {

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    PlayMusicAdapter mAdapter;

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case "com.android.music.changepage":
                    int page = intent.getIntExtra("page", 0);
                    mViewPager.setCurrentItem(page);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter iF = new IntentFilter();
        iF.addAction("com.android.music.changepage");
        registerReceiver(mReceiver, iF);
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.layout_container;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new PlayMusicAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(mAdapter.getCount() - 1);
    }
}
