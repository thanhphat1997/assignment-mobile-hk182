package vn.edu.hcmut.phatdo.assignment_mobile_182.playlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import butterknife.BindView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.music.SongListAdapter;
import vn.edu.hcmut.phatdo.assignment_mobile_182.utils.SongUtils;

public class AddSongActivity extends BaseActivity {

    SongListAdapter mAdapter;

    List<SongModel> mSongList = new ArrayList<>();

    List<SongModel> mSelected = new ArrayList<>();

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.tv_count)
    TextView tvCount;

    SelectionTracker<Long> mTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSongList();
        mAdapter = new SongListAdapter(getApplicationContext(), mSongList, true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mTracker = new SelectionTracker.Builder<>(
                "mySelection",
                mRecyclerView,
                new StableIdKeyProvider(mRecyclerView),
                new MyItemDetailsLookup(mRecyclerView),
                StorageStrategy.createLongStorage()
        ).withSelectionPredicate(
                SelectionPredicates.<Long>createSelectAnything()
        ).build();
        mTracker.addObserver(new SelectionTracker.SelectionObserver() {
            @Override
            public void onItemStateChanged(@NonNull Object key, boolean selected) {
                super.onItemStateChanged(key, selected);
                int size = mTracker.getSelection().size();
                String text = size + " items selected";
                tvCount.setText(text);
            }
        });
        mAdapter.setTracker(mTracker);
    }

    private void getSongList() {
        Bundle bundle = getIntent().getExtras();
        List<SongModel> list = bundle.getParcelableArrayList("songList");
        mSongList = SongUtils.getSong(list);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_song, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_add:
                Selection selection = mTracker.getSelection();
                Iterator i = selection.iterator();
                while (i.hasNext()) {
                    long index = (long) i.next();
                    mSelected.add(mSongList.get((int) index));
                }
                Intent intent = new Intent();
                intent.putParcelableArrayListExtra("result", (ArrayList<? extends Parcelable>) mSelected);
                setResult(Activity.RESULT_OK, intent);
                finish();
                return true;
        }
        return false;
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.layout_add_song;
    }
}
