package vn.edu.hcmut.phatdo.assignment_mobile_182.music;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseFragment;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SongDBModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.music.PlayMusicActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.playlist.PlaylistActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.utils.SongUtils;

public class LocalMusicFragment extends BaseFragment {

    LocalMusicPresenter mPresenter;
    LocalMusicAdapter mAdapter;
    List<SongModel> mSongList = new ArrayList<>();
    SQLiteDBHelper mDBHelper;

    @BindView(R.id.recent_songs)
    RecyclerView mRecyclerView;

    @BindView(R.id.tv_num_song)
    TextView mTVNumSong;

    @BindView(R.id.tv_num_playlist)
    TextView mTVNumPlaylist;

    @BindView(R.id.tv_num_lovesong)
    TextView mTVNumLoveSong;

    @OnClick(R.id.rl_song)
    public void onClickSongs() {
        Intent intent = new Intent(getContext(), PlayMusicActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_play_list)
    public void onClickPlaylist() {
        Intent intent = new Intent(getContext(), PlaylistActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_love_songs)
    public void onClickLoveSong() {
        Intent intent = new Intent(getContext(), PlayMusicActivity.class);
        intent.setAction("lovesong");
        startActivity(intent);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.layout_local_music;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter = new LocalMusicPresenter();
        mPresenter.onAttach(this);
        mDBHelper = new SQLiteDBHelper(getContext());
        mAdapter = new LocalMusicAdapter(mSongList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void updateNum() {
        int numSong = SongUtils.mSongList.size();
        int numPlaylist = mDBHelper.readAllPlaylist().size();
        int numLoveSong = mDBHelper.getAllLoveSong().size();
        mTVNumSong.setText(String.valueOf(numSong));
        mTVNumPlaylist.setText(String.valueOf(numPlaylist));
        mTVNumLoveSong.setText(String.valueOf(numLoveSong));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        Uri chayNgayDi = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
//                "://" + getResources().getResourcePackageName(R.drawable.sontung)
//                + '/' + getResources().getResourceTypeName(R.drawable.sontung) + '/' + getResources().getResourceEntryName(R.drawable.sontung));
//        Uri unknown = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
//                "://" + getResources().getResourcePackageName(R.drawable.unknown)
//                + '/' + getResources().getResourceTypeName(R.drawable.unknown) + '/' + getResources().getResourceEntryName(R.drawable.unknown) );
//        mSongList.add(new SongModel(0, chayNgayDi, "Chạy ngay đi", "Sơn Tùng M-TP", ""));
//        mSongList.add(new SongModel(1, unknown, "Shape Of You", "Ed Shareen", ""));
//        mSongList.add(new SongModel(2, chayNgayDi, "Chạy ngay đi", "Sơn Tùng M-TP", ""));
//        mSongList.add(new SongModel(3, unknown, "Shape Of You", "Ed Shareen", ""));

    }

    @Override
    public void onResume() {
        super.onResume();
        updateNum();
        List<SongDBModel> list = mDBHelper.getAllSong();
        mSongList.clear();
        mSongList.addAll(SongUtils.getRecentSong(list));
        mAdapter.setDataList(mSongList);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
