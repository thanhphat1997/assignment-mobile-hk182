package vn.edu.hcmut.phatdo.assignment_mobile_182.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import vn.edu.hcmut.phatdo.assignment_mobile_182.ProfileFragment;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.AbsPagerAdapter;
import vn.edu.hcmut.phatdo.assignment_mobile_182.music.LocalMusicFragment;
import vn.edu.hcmut.phatdo.assignment_mobile_182.news.NewsFragment;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.video.LocalVideosFragment;
import vn.edu.hcmut.phatdo.assignment_mobile_182.top.TopFragment;

public class HomeAdapter extends AbsPagerAdapter {

    private final int PAGE_COUNT = 5;

    public HomeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case HomePage.MUSIC:
                return new LocalMusicFragment();
            case HomePage.VIDEO:
                return new LocalVideosFragment();
            case HomePage.TOP:
                return new TopFragment();
            case HomePage.NEWS:
                return new NewsFragment();
            case HomePage.PROFILE:
                return new ProfileFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}

