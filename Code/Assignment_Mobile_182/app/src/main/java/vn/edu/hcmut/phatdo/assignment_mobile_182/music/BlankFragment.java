package vn.edu.hcmut.phatdo.assignment_mobile_182.music;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseFragment;

public class BlankFragment extends BaseFragment {

    @Override
    protected int getResLayoutId() {
        return R.layout.song_item;
    }
}
