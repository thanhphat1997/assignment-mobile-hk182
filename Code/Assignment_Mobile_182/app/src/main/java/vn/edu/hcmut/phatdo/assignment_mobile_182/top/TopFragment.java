package vn.edu.hcmut.phatdo.assignment_mobile_182.top;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.video.VideoRecyclerViewAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopFragment extends Fragment {


    ArrayList<MyVideo> topVideos = new ArrayList<>();
    ArrayList<MySong> topSongs = new ArrayList<>();
    TopVideoAdapter topVideoAdapter;
    TopSongAdapter topSongAdapter;
    RecyclerView rvTopVideos;
    RecyclerView rvTopSongs;

    public TopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v =  inflater.inflate(R.layout.fragment_top, container, false);



        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadSong();
        loadVideo();
    }

    private void loadSong(){
        final Activity activity = getActivity();
        rvTopSongs = activity.findViewById(R.id.rv_top_songs);
        topSongAdapter = new TopSongAdapter(topSongs, new VideoRecyclerViewAdapter.CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                MySong song = topSongs.get(position);
                Intent intent = new Intent(activity, WebviewActivity.class);
                intent.putExtra("link", song.link);
                startActivity(intent);
            }
        });
        rvTopSongs.setAdapter(topSongAdapter);
        rvTopSongs.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference myRef = db.getReference().child("listsong");
        final ArrayList<MyVideo> listVideo = new ArrayList<>();
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (topSongs.size() > 0){
                    for (int i = 0; i<topSongs.size();i++)
                        topSongs.remove(i);
                }
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    MySong video = singleSnapshot.getValue(MySong.class);
                    topSongs.add(video);
                    topSongAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

    private void loadVideo(){
        Activity activity = getActivity();
        rvTopVideos = activity.findViewById(R.id.rv_top_videos);
        topVideoAdapter = new TopVideoAdapter(topVideos);
        rvTopVideos.setAdapter(topVideoAdapter);
        rvTopVideos.setLayoutManager(new GridLayoutManager(getContext(),2));

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference myRef = db.getReference().child("listvideo");
        final ArrayList<MyVideo> listVideo = new ArrayList<>();
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (topVideos.size()>0){
                    for (int i = 0; i<topVideos.size();i++)
                        topVideos.remove(i);
                }
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                    MyVideo video = singleSnapshot.getValue(MyVideo.class);
                    topVideos.add(video);
                    topVideoAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
    }

}
