package vn.edu.hcmut.phatdo.assignment_mobile_182.top;

import java.io.Serializable;

public class MySong implements Serializable {
    public String title;
    public String imgSrc;
    public String link;
    public String artist;

    public MySong(){}
}
