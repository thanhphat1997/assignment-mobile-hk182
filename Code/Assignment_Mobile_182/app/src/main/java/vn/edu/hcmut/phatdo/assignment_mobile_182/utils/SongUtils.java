package vn.edu.hcmut.phatdo.assignment_mobile_182.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SongDBModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;

public class SongUtils {

    public static List<SongModel> mSongList = new ArrayList<>();

    private static List<Long> albumNoArt = new ArrayList<>();

    public static void getSongList(Context context) {
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(songUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            //get columns
            int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int idColumn = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int pathColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            //add songs to list
            do {
                long id = cursor.getLong(idColumn);
                String thisTitle = cursor.getString(titleColumn);
                String thisArtist = cursor.getString(artistColumn);
                long albumId = cursor.getLong(albumColumn);
                Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
                Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId);
                albumArtUri = checkAlbumArt(context, albumArtUri, albumId);
                String path = cursor.getString(pathColumn);
                mSongList.add(new SongModel(id, albumArtUri, thisTitle, thisArtist, path));
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        sort(mSongList);
    }

    public static SongModel getSong(int position) {
        return mSongList.get(position);
    }

    private static Uri checkAlbumArt(Context context, Uri uri, long albumId) {
        if (albumNoArt.contains(albumId)) {
            return getDefaultAlbumArt(context);
        }

        try {
            context.getContentResolver().openFileDescriptor(uri, "r");
        } catch (FileNotFoundException e) {
            albumNoArt.add(albumId);
            return getDefaultAlbumArt(context);
        }
        return uri;
    }

    private static Uri getDefaultAlbumArt(Context context) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + context.getResources().getResourcePackageName(R.drawable.default_album)
                + '/' + context.getResources().getResourceTypeName(R.drawable.default_album)
                + '/' + context.getResources().getResourceEntryName(R.drawable.default_album));
    }

    public static List<SongModel> getSong(List<SongModel> songList) {
        List<SongModel> list = new ArrayList<>();
        for (SongModel song : mSongList) {
            boolean has = false;
            for (SongModel s : songList) {
                if (s.getId() == song.getId()) {
                    has = true;
                    break;
                }
            }
            if (!has) {
                list.add(song);
            }
        }
        return list;
    }

    public static List<SongModel> getSongList(List<Long> idList) {
        Collections.sort(mSongList, new Comparator<SongModel>() {
            @Override
            public int compare(SongModel lhs, SongModel rhs) {
                return Long.compare(lhs.getId(), rhs.getId());
            }
        });

        List<SongModel> songList = new ArrayList<>();
        for (SongModel song : mSongList) {
            for (int i = 0; i < idList.size(); i++) {
                if (song.getId() == idList.get(i)) {
                    songList.add(song);
                    break;
                }
            }
        }

        sort(songList);

        sort(mSongList);

        return songList;
    }

    public static void sort(List<SongModel> songList) {
        Collections.sort(songList, new Comparator<SongModel>() {
            @Override
            public int compare(SongModel lhs, SongModel rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
    }

    public static List<SongModel> getRecentSong(List<SongDBModel> dbList) {
        List<SongModel> list = new ArrayList<>();
        Collections.sort(dbList, new Comparator<SongDBModel>() {
            @Override
            public int compare(SongDBModel o1, SongDBModel o2) {
                return Long.compare(o2.timestamp, o1.timestamp);
            }
        });
        if (dbList.size() > 5) {
            dbList = dbList.subList(0, 5);
        }

        for (SongDBModel dbSong : dbList) {
            for (SongModel song : mSongList) {
                if (dbSong.id == song.getId()) {
                    list.add(song);
                    break;
                }
            }
        }
        return list;
    }
}
