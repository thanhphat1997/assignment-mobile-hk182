package vn.edu.hcmut.phatdo.assignment_mobile_182.news;

import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoSnippet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class VideoData {
    private Video mVideo;

    public String getChannelImageURL() {
        return channelImageURL;
    }

    public void setChannelImageURL(String channelImageURL) {
        this.channelImageURL = channelImageURL;
    }

    private String channelImageURL;
    public VideoData(Video video){
        mVideo = video;
    }
    public Video getVideo() {
        return mVideo;
    }

    public void setVideo(Video video) {
        mVideo = video;
    }

    public String getYouTubeId() {
        return mVideo.getId();
    }

    public String getTitle() {
        return mVideo.getSnippet().getTitle();
    }

    public String getLikes(){
        return   String.valueOf(mVideo.getStatistics().getLikeCount());
    }
    public  String getViewCount(){
        return mVideo.getStatistics().getViewCount().toString();
    }
    public String getChannelId(){
        return  mVideo.getSnippet().getChannelId();
    }
    public String getChannelTitle(){
        return  mVideo.getSnippet().getChannelTitle();
    }
    public Date getDate(){
        String dt =  mVideo.getSnippet().getPublishedAt().toString();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        Date d= null;
        try {
            String date;
            d = formatter.parse(dt);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return d;
    }
    public VideoSnippet addTags(Collection<? extends String> tags) {
        VideoSnippet mSnippet = mVideo.getSnippet();
        List<String> mTags = mSnippet.getTags();
        if (mTags == null) {
            mTags = new ArrayList<String>(2);
        }
        mTags.addAll(tags);
        return mSnippet;
    }

    public String getThumbUri() {
        return mVideo.getSnippet().getThumbnails().getHigh().getUrl();
    }

    public String getWatchUri() {
        return "http://www.youtube.com/watch?v=" + getYouTubeId();
    }

    public News toNews(){
        return new News(
                getChannelTitle(),
                getDate(),
                getTitle(),
                getChannelImageURL(),
                getThumbUri(),
                getWatchUri()
        );
    }
}
