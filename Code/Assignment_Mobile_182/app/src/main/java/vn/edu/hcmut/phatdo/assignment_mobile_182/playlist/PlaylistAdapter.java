package vn.edu.hcmut.phatdo.assignment_mobile_182.playlist;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.AbsViewHolder;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.PlaylistModel;


public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder> {

    private List<PlaylistModel> mDataList;

    private PlaylistListener mListener;

    private SelectionTracker mTracker;

    private Context mContext;

    public PlaylistAdapter(List<PlaylistModel> dataList) {
        this.mDataList = dataList;
    }

    public PlaylistAdapter(Context context, List<PlaylistModel> dataList, boolean hasStableId) {
        this.mContext = context;
        this.mDataList = dataList;
        setHasStableIds(hasStableId);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @NonNull
    @Override
    public PlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.playlist_item, parent, false);
        return new PlaylistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PlaylistViewHolder holder, final int position) {
        if (mTracker != null) {
            if (mTracker.isSelected((long) position)) {
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.selected));
            } else {
                holder.itemView.setBackgroundColor(Color.WHITE);
            }
        }
        holder.tvName.setText(mDataList.get(position).name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener == null) {
                    return;
                }
                mListener.onClickItem(position);
            }
        });
    }

    public void setDataList(List<PlaylistModel> songList) {
        mDataList = songList;
        notifyDataSetChanged();
    }

    public class PlaylistViewHolder extends AbsViewHolder {
        View mView;
        TextView tvName;

        PlaylistViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            tvName = mView.findViewById(R.id.tv_name);
        }

        @Override
        public ItemDetailsLookup.ItemDetails<Long> getItemDetails() {
            return new ItemDetailsLookup.ItemDetails<Long>() {
                @Override
                public int getPosition() {
                    return getAdapterPosition();
                }

                @Override
                public Long getSelectionKey() {
                    return getItemId();
                }
            };
        }
    }

    public void setListener(PlaylistListener listener) {
        mListener = listener;
    }

    public void setTracker(SelectionTracker tracker) {
        this.mTracker = tracker;
    }

    interface PlaylistListener {
        void onClickItem(int position);
    }
}

