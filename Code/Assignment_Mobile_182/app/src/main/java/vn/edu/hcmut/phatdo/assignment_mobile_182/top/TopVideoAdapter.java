package vn.edu.hcmut.phatdo.assignment_mobile_182.top;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import io.github.ponnamkarthik.richlinkpreview.MetaData;
import io.github.ponnamkarthik.richlinkpreview.ResponseListener;
import io.github.ponnamkarthik.richlinkpreview.RichPreview;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;


// Note that we specify the custom ViewHolder which gives us access to our views
public class TopVideoAdapter extends
        RecyclerView.Adapter<TopVideoAdapter.ViewHolder>  {

    // Store a member variable for the contacts
    private List<MyVideo> items ;

    // Pass in the contact array into the constructor
    public TopVideoAdapter(List<MyVideo> list) {
        items = list;
    }

    public void addMore(List<MyVideo> newItems) {
        int last = items.size();
        items.addAll(newItems);
        notifyItemRangeInserted(last,newItems.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_top_video, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        // Get the data model based on position
        final MyVideo topVideo = items.get(position);

        // Set item views based on your views and data model
        holder.name.setText(topVideo.title);
        holder.preview.getPreview(topVideo.link);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView name;
        public RichPreview preview;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            final View view = itemView;

            name = view.findViewById(R.id.top_video_name);
            preview = new RichPreview(new ResponseListener() {
                @Override
                public void onData(final MetaData metaData) {
                    String imageURL = metaData.getImageurl();
                    if(imageURL == "") return;
                    ImageView iv = view.findViewById(R.id.top_video_image);
                    Picasso.get().load(imageURL).centerCrop().fit().into(iv);
                    iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(metaData.getUrl()));
                            view.getContext().startActivity(browserIntent);
                        }
                    });
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }
    }



}

