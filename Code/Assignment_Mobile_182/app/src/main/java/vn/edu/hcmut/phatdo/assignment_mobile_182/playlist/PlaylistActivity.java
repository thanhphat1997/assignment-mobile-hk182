package vn.edu.hcmut.phatdo.assignment_mobile_182.playlist;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.api.services.youtube.model.Playlist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import butterknife.BindView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.PlaylistModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;

public class PlaylistActivity extends BaseActivity implements PlaylistAdapter.PlaylistListener{

    PlaylistAdapter mAdapter;

    private List<PlaylistModel> mList;

    SQLiteDBHelper sqLiteDBHelper;

    SelectionTracker<Long> mTracker;

    @BindView(R.id.list)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sqLiteDBHelper = new SQLiteDBHelper(this);
        mList = sqLiteDBHelper.readAllPlaylist();
        mAdapter = new PlaylistAdapter(getApplicationContext(), mList, true);
        mAdapter.setListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mTracker = new SelectionTracker.Builder<>(
                "mySelection",
                mRecyclerView,
                new StableIdKeyProvider(mRecyclerView),
                new MyItemDetailsLookup(mRecyclerView),
                StorageStrategy.createLongStorage()
        ).withSelectionPredicate(
                SelectionPredicates.<Long>createSelectAnything()
        ).build();
        mAdapter.setTracker(mTracker);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.layout_playlist;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_playlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_add:
                showDialog();
                return true;
            case R.id.item_delete:
                List<PlaylistModel> selected = new ArrayList<>();
                Selection selection = mTracker.getSelection();
                Iterator i = selection.iterator();
                while (i.hasNext()) {
                    long index = (long) i.next();
                    selected.add(mList.get((int) index));
                }
                for (PlaylistModel playlist : selected) {
                    mList.remove(playlist);
                    mAdapter.notifyDataSetChanged();
                }
                sqLiteDBHelper.removePlaylist(selected);
                mTracker.clearSelection();
                return true;
        }
        return false;
    }

    private void showDialog() {
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(input)
                .setTitle("Enter playlist name")
                .setPositiveButton("OK", null)
                .setNegativeButton("CANCEL", null)
                .create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        String name = input.getText().toString();
                        if (TextUtils.isEmpty(name)) {
                            input.setError("Playlist name is invalid");
                            return;
                        }

                        sqLiteDBHelper.insertPlaylist(name);
                        mList = sqLiteDBHelper.readAllPlaylist();
                        Collections.sort(mList, new Comparator<PlaylistModel>() {
                            @Override
                            public int compare(PlaylistModel lhs, PlaylistModel rhs) {
                                return lhs.name.compareTo(rhs.name);
                            }
                        });
                        mAdapter.setDataList(mList);
                        mAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
            }
        });
        alertDialog.show();
    }

    @Override
    public void onClickItem(int position) {
        Intent intent = new Intent(this, PlaylistDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("playlist", mList.get(position));
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
}
