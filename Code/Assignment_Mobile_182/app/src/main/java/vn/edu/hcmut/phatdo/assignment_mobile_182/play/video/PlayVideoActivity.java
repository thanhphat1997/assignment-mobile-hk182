package vn.edu.hcmut.phatdo.assignment_mobile_182.play.video;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.concurrent.TimeUnit;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;

public class PlayVideoActivity extends AppCompatActivity {

    private VideoView videoPlayer;
    private Button btnPlay;
    private ConstraintLayout playBar;
    private View hiddenView;
    private int stopPosition = 0;
    MediaPlayer mp;
    private SeekBar seekBar;
    private Handler mHandler = new Handler();
    private TextView txtDuration, txtProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_play_video);

        findViewAndSetup();
        playVideo();

    }

    private void findViewAndSetup(){
        //Bind views
        videoPlayer = findViewById(R.id.videoPlayer);
        btnPlay = findViewById(R.id.btnPlay);
        playBar = findViewById(R.id.playBarVideo);
        hiddenView = findViewById(R.id.viewVideoPlayer);
        seekBar = findViewById(R.id.video_seek_bar);
        txtDuration = findViewById(R.id.txtDuration);
        txtProgress = findViewById(R.id.txtProgress);
        //Setup
        playBar.setVisibility(ConstraintLayout.INVISIBLE);

        hiddenView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int visibility = playBar.getVisibility();
                if(visibility == ConstraintLayout.VISIBLE){
                    playBar.setVisibility(ConstraintLayout.INVISIBLE);
                }
                else{
                    playBar.setVisibility(ConstraintLayout.VISIBLE);
                }
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp.isPlaying()){
                    btnPlay.setBackground(getDrawable(R.drawable.ic_resume));
                    stopPosition = mp.getCurrentPosition();
                    mp.pause();
                }
                else {
                    btnPlay.setBackground(getDrawable(R.drawable.ic_pause));
                    if (mp!=null){
                        mp.seekTo(stopPosition);
                        mp.start();
                    }

                }
            }
        });


    }
    private String getVideoLengthString(int duration){

        /*convert millis to appropriate time*/
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
        );
    }

    private void playVideo(){
        VideoModel model = (VideoModel) getIntent().getSerializableExtra("path");
        if (model != null){
            Uri uri = Uri.parse(model.getVideoPath());
            videoPlayer.setVideoURI(uri);
            videoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mp = mediaPlayer;
                    mp.start();
                    seekBar.setMax(mp.getDuration());
                    txtDuration.setText(getVideoLengthString(mp.getDuration()));
                    PlayVideoActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if(mp != null){
                                int currentPosition = mp.getCurrentPosition();
                                seekBar.setProgress(currentPosition);
                                txtProgress.setText(getVideoLengthString(currentPosition));
                            }
                            mHandler.postDelayed(this, 1000);
                        }
                    });
                    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if(mp != null && fromUser){
                                mp.seekTo(progress);
                                txtProgress.setText(getVideoLengthString(progress));
                            }
                        }
                    });
                }
            });

//            videoPlayer.start();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        videoPlayer.pause();
        mHandler.removeCallbacksAndMessages(null);
    }
//
    @Override
    protected void onStop() {
        super.onStop();
        videoPlayer.pause();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoPlayer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoPlayer.seekTo(stopPosition);
        videoPlayer.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoPlayer.pause();
        stopPosition = videoPlayer.getCurrentPosition();
    }
//
    @Override
    protected void onRestart() {
        super.onRestart();
        videoPlayer.resume();
    }
}
