package vn.edu.hcmut.phatdo.assignment_mobile_182;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.login.LoginActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    TextView txtProfileName, txtSignIn, txtEmail;
    CircleImageView imgProfile;
    ImageView imgVipMem;
    ConstraintLayout logout_layout;
    private FirebaseAuth mAuth;
    Activity activity;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.layout_personal, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setUp();

    }

    private void setUp(){
        activity = getActivity();
        txtProfileName = activity.findViewById(R.id.txt_profile_name);
        txtSignIn = activity.findViewById(R.id.txt_sign_in);
        imgProfile = activity.findViewById(R.id.img_user_profile);
        imgVipMem = activity.findViewById(R.id.img_vip_member);
        logout_layout = activity.findViewById(R.id.logout_layout);
        txtEmail = activity.findViewById(R.id.txt_profile_email);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null){
            updateView();
        }

        logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(activity, LoginActivity.class);
                mAuth.signOut();
                LoginManager.getInstance().logOut();
                startActivity(loginIntent);
                activity.finish();
            }
        });



    }

    private void updateView(){
        FirebaseUser user = mAuth.getCurrentUser();
        txtProfileName.setText(user.getDisplayName());
        txtSignIn.setText(R.string.logout);
        txtEmail.setText(user.getEmail());
        new ProfileFragment.DownloadImageTask(imgProfile).execute(user.getPhotoUrl().toString()+ "?height=100");
        imgVipMem.setVisibility(View.VISIBLE);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
