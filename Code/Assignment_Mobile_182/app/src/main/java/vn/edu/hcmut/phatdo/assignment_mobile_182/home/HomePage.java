package vn.edu.hcmut.phatdo.assignment_mobile_182.home;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({HomePage.MUSIC, HomePage.VIDEO, HomePage.TOP, HomePage.NEWS, HomePage.PROFILE})
@Retention(RetentionPolicy.SOURCE)
public @interface HomePage {
    int MUSIC = 0;
    int VIDEO = 1;
    int TOP = 2;
    int NEWS = 3;
    int PROFILE = 4;
}
