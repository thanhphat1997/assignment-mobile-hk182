package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.SeekBar;
import android.widget.TextView;

import com.like.LikeButton;
import com.like.OnLikeListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseFragment;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;
import vn.edu.hcmut.phatdo.assignment_mobile_182.utils.SongUtils;

public class PlayMusicFragment extends BaseFragment {

    @BindView(R.id.seekbar)
    SeekBar mSeekBar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_singer)
    TextView tvSinger;

    @BindView(R.id.ic_song)
    CircleImageView icSong;

    @BindView(R.id.btn_play)
    FloatingActionButton mBtnPlay;

    @BindView(R.id.tv_seakbar)
    TextView mTvSeakbar;

    @BindView(R.id.btn_love)
    LikeButton mLike;

    private Animation mAnimation;

    private List<SongModel> mSongList;

    private List<Long> mLoveSong;

    private SQLiteDBHelper mDBHelper;

    private int mCurrent;

    @OnClick(R.id.btn_play)
    public void onClickPlay() {
        play();
    }

    @OnClick(R.id.btn_next)
    public void onClickNext() {
        next();
    }

    @OnClick(R.id.btn_previous)
    public void onClickPrevious() {
        previous();
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case "com.android.music.complete":
                    next();
                    break;
                case "com.android.music.duration":
                    int duration = intent.getIntExtra("duration", 0);
                    updateDuration(duration);
                    break;
                case "com.android.music.updateprogress":
                    int progress = intent.getIntExtra("progress", 0);
                    updateProgress(progress);
                    break;
                case "com.android.music.updatestatus":
                    boolean status = intent.getBooleanExtra("status", false);
                    updateStatus(status);
                    break;
                case "com.android.music.song":
                    int id = intent.getIntExtra("song", 0);
                    updateSong(id);
                    break;
                default:
                    break;
            }
        }
    };

    private void updateSong(int id) {
        if (mSongList == null || mSongList.isEmpty()) {
            return;
        }
        mCurrent = id;
        SongModel song = mSongList.get(id);
        tvName.setText(song.getName());
        tvSinger.setText(song.getSinger());
        icSong.setImageURI(song.getIcon());
        mLike.setLiked(false);
        for (long songId : mLoveSong) {
            if (songId == mSongList.get(id).getId()) {
                mLike.setLiked(true);
                break;
            }
        }
    }

    private void updateStatus(boolean status) {
        if (status) {
            mBtnPlay.setForeground(ContextCompat.getDrawable(getContext(), android.R.drawable.ic_media_pause));
            if (icSong.getAnimation() == null) {
                icSong.startAnimation(mAnimation);
            }
            return;
        }

        mBtnPlay.setForeground(ContextCompat.getDrawable(getContext(), android.R.drawable.ic_media_play));
        icSong.clearAnimation();
    }

    private void updateDuration(int duration) {
        mSeekBar.setMax(duration);
    }

    private void updateProgress(int progress) {
        mSeekBar.setProgress(progress);
    }

    @Override
    protected int getResLayoutId() {
        return R.layout.layout_play_music;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDBHelper = new SQLiteDBHelper(getContext());
        mLoveSong = mDBHelper.getAllLoveSong();
        Intent acIntent = getActivity().getIntent();
        String action = acIntent.getAction();
        if (action == null) {
            action = "";
        }
        updateSongList(action);
        mAnimation = AnimationUtils.loadAnimation(getContext(),R.anim.rotate_animation);
        mAnimation.setRepeatCount(Animation.INFINITE);
        Intent intent = new Intent(getContext(), MediaPlayerService.class);
        intent.setAction(action);
        intent.putExtra("action", MediaPlayerService.ACTION_PLAY);
        intent.putParcelableArrayListExtra("songList", (ArrayList<? extends Parcelable>) mSongList);
        getActivity().startService(intent);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                if (!fromUser) {
                    return;
                }

                Intent intent = new Intent();
                intent.setAction("com.android.music.command");
                intent.putExtra("action", "progress");
                intent.putExtra("progress", mSeekBar.getProgress());
                getActivity().sendBroadcast(intent);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mLike.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                mDBHelper.insertLoveSong(mSongList.get(mCurrent));
                mLoveSong = mDBHelper.getAllLoveSong();
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                mDBHelper.removeLoveSong(mSongList.get(mCurrent));
                mLoveSong = mDBHelper.getAllLoveSong();
            }
        });
    }

    private void updateSongList(String action) {
        if (action.equalsIgnoreCase("playlist")) {
            mSongList = getActivity().getIntent().getParcelableArrayListExtra("songList");
        } else if (action.equalsIgnoreCase("lovesong")) {
            mSongList = SongUtils.getSongList(mDBHelper.getAllLoveSong());
        } else {
            mSongList = SongUtils.mSongList;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void play() {
        if (mSongList == null || mSongList.isEmpty()) {
            return;
        }

        Intent intent = new Intent();
        intent.setAction("com.android.music.command");
        intent.putExtra("action", "play");
        getActivity().sendBroadcast(intent);
    }

    private void next() {
        if (mSongList == null || mSongList.isEmpty()) {
            return;
        }

        mSeekBar.setProgress(0);
        Intent intent = new Intent();
        intent.setAction("com.android.music.command");
        intent.putExtra("action", "next");
        getActivity().sendBroadcast(intent);
    }

    private void previous() {
        if (mSongList == null || mSongList.isEmpty()) {
            return;
        }

        mSeekBar.setProgress(0);
        Intent intent = new Intent();
        intent.setAction("com.android.music.command");
        intent.putExtra("action", "previous");
        getActivity().sendBroadcast(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter iF = new IntentFilter();
        iF.addAction("com.android.music.complete");
        iF.addAction("com.android.music.duration");
        iF.addAction("com.android.music.updatestatus");
        iF.addAction("com.android.music.updateprogress");
        iF.addAction("com.android.music.song");
        getActivity().registerReceiver(mReceiver, iF);
        Intent intent = new Intent();
        intent.setAction("com.android.music.command");
        intent.putExtra("action", "starttimer");
        getActivity().sendBroadcast(intent);
        Intent status = new Intent();
        status.setAction("com.android.music.command");
        status.putExtra("action", "status");
        getActivity().sendBroadcast(status);
    }

    @Override
    public void onStop() {
        super.onStop();
        Intent intent = new Intent();
        intent.setAction("com.android.music.command");
        intent.putExtra("action", "stoptimer");
        getActivity().sendBroadcast(intent);
        getActivity().unregisterReceiver(mReceiver);
    }
}
