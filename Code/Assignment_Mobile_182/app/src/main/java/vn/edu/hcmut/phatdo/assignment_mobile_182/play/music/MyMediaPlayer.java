package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.media.MediaPlayer;

public class MyMediaPlayer extends MediaPlayer {

    private static MyMediaPlayer mediaPlayer = new MyMediaPlayer();
    public static boolean release = false;

    private MyMediaPlayer() {

    }

    public static MyMediaPlayer getInstance() {
        if (release) {
            mediaPlayer = new MyMediaPlayer();
        }
        return mediaPlayer;
    }
}
