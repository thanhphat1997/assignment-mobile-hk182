package vn.edu.hcmut.phatdo.assignment_mobile_182.play.video;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;

import static butterknife.internal.Utils.arrayOf;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocalVideosFragment extends Fragment {

    private final int VIDEO_CODE = 1997;
    RecyclerView lvVideoList;
    ArrayList<VideoModel> listVideoModel;
    VideoRecyclerViewAdapter adapter;

    public LocalVideosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_local_videos, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lvVideoList = getActivity().findViewById(R.id.lvVideo);
        checkPermission();
    }



    private void checkPermission(){
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), VIDEO_CODE);
            return;
        }
        loadListVideo();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == VIDEO_CODE && grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            loadListVideo();
        }
    }

    private void loadListVideo(){
        ArrayList<String> listVideos = getAllMedia();
        listVideoModel = new ArrayList<>();
        for(int i=0; i<listVideos.size();i++){
            String videoPath = listVideos.get(i);
            int duration = getVideoLength(videoPath);
            VideoModel videoModel = new VideoModel(getVideoNameFromUri(videoPath), getVideoLengthString(duration), videoPath, duration);
            listVideoModel.add(videoModel);
        }

        adapter = new VideoRecyclerViewAdapter(listVideoModel, new VideoRecyclerViewAdapter.CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                VideoModel model = listVideoModel.get(position);
                Intent intent = new Intent(getContext(), PlayVideoActivity.class);
                intent.putExtra("path", model);
                startActivity(intent);
            }
        });
        lvVideoList.setAdapter(adapter);
        lvVideoList.setLayoutManager(new GridLayoutManager(getActivity(),2));
    }

    public ArrayList<String> getAllMedia() {
        HashSet<String> videoItemHashSet = new HashSet<>();
        String[] projection = { MediaStore.Video.VideoColumns.DATA ,MediaStore.Video.Media.DISPLAY_NAME};
        Cursor cursor = getContext().getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
        try {
            cursor.moveToFirst();
            do{
                videoItemHashSet.add((cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA))));
            }while(cursor.moveToNext());

            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<String> downloadedList = new ArrayList<>(videoItemHashSet);
        return downloadedList;
    }

    private String getVideoNameFromUri(String path) {
        String[] pathArray = path.split("/");
        return pathArray[pathArray.length - 1];
    }

    private String getVideoLengthString(int duration){

        /*convert millis to appropriate time*/
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
        );
    }

    private int getVideoLength(String path){
        MediaPlayer mp = MediaPlayer.create(getContext(), Uri.parse(path));
        int duration = mp.getDuration();
        mp.release();
        return duration;
    }


    @Override
    public void onStart() {
        super.onStart();
//        checkPermission();
//        setListVideoListener();
    }
}
