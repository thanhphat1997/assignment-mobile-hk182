package vn.edu.hcmut.phatdo.assignment_mobile_182.music;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.model.SongModel;


public class LocalMusicAdapter extends RecyclerView.Adapter<LocalMusicAdapter.MusicViewHolder> {

    private List<SongModel> mDataList;

    public LocalMusicAdapter(List<SongModel> dataList) {
        this.mDataList = dataList;
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @NonNull
    @Override
    public MusicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.song_item, parent, false);
        return new MusicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicViewHolder holder, int position) {
        holder.tvName.setText(mDataList.get(position).getName());
        holder.tvSinger.setText(mDataList.get(position).getSinger());
        holder.icSong.setImageURI(mDataList.get(position).getIcon());
    }

    public void setDataList(List<SongModel> songList) {
        mDataList = songList;
        notifyDataSetChanged();
    }

    class MusicViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView tvSinger;
        TextView tvName;
        ImageView icSong;

        MusicViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            tvName = mView.findViewById(R.id.tv_name);
            tvSinger = mView.findViewById(R.id.tv_singer);
            icSong = mView.findViewById(R.id.ic_song);
        }
    }
}
