package vn.edu.hcmut.phatdo.assignment_mobile_182.news;

import android.app.Activity;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import java.util.ArrayList;
import java.util.List;

public class EndlessNewsScrollingListener implements ViewTreeObserver.OnScrollChangedListener {

    NestedScrollView mScrollView;
    boolean firstLoad ;
    NewsAdapter adapter;
    Activity activity;
    public EndlessNewsScrollingListener(Activity activity,NestedScrollView mScrollView, NewsAdapter adapter){
        this.mScrollView = mScrollView;
        firstLoad = true;
        this.adapter = adapter;
        this.activity = activity;
    }
    @Override
    public void onScrollChanged() {
        if(adapter.getItemCount()==0){
            if(firstLoad) {
                onLoadMore();
                firstLoad = false;
            }
        }
        else {
            View view = (View) mScrollView.getChildAt(mScrollView.getChildCount() - 1);

            int diff = (view.getBottom() - (mScrollView.getHeight() + mScrollView
                    .getScrollY()));

            if (diff <= 0) {
                onLoadMore();
            }
        }
    }

    // Defines the process for actually loading more data based on page
    public void onLoadMore(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    VideoResponse videoResponse = YoutubeApi.getVideoResponse(adapter.pageToken);
                    adapter.pageToken = videoResponse.nextPageToken;
                    Log.e("TOKEN",videoResponse.nextPageToken);
                    final List<News> newsList = new ArrayList<>();
                    for (VideoData videoData : videoResponse.videoDataList) {
                        newsList.add(videoData.toNews());
                    }

                    //update recycler view
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.addMore(newsList);
                        }
                    });
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }

}
