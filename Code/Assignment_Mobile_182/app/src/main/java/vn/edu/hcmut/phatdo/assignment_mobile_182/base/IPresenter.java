package vn.edu.hcmut.phatdo.assignment_mobile_182.base;

public interface IPresenter<T> {
    void onAttach(T pView);

    void onDetach();
}