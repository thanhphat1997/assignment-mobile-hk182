package vn.edu.hcmut.phatdo.assignment_mobile_182.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.InputStream;

import butterknife.BindView;
import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.base.BaseActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.data.SQLiteDBHelper;
import vn.edu.hcmut.phatdo.assignment_mobile_182.login.LoginActivity;
import vn.edu.hcmut.phatdo.assignment_mobile_182.play.music.MediaPlayerService;
import vn.edu.hcmut.phatdo.assignment_mobile_182.utils.SongUtils;
import vn.edu.hcmut.phatdo.assignment_mobile_182.widget.NonSwipeableViewPager;

import static butterknife.internal.Utils.arrayOf;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private HomeAdapter mAdapter;
    private FirebaseAuth mAuth;

    @BindView(R.id.view_pager)
    NonSwipeableViewPager mViewPager;

    @BindView(R.id.navigation)
    BottomNavigationView mBottomView;

    private SQLiteDBHelper mDBHelper;

    @Override
    protected int getResLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAdapter = new HomeAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(mAdapter.getCount() - 1);
        mAuth = FirebaseAuth.getInstance();
        mDBHelper = new SQLiteDBHelper(this);

//        FirebaseMessaging.getInstance().subscribeToTopic("weather")
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        String msg = "Subscribed";
//                        if (!task.isSuccessful()) {
//                            msg = "Failed to subscribe";
//                        }
//                        Log.d("BKNoti", msg);
//                        Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
//                    }
//                });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(getColor(R.color.gray));
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mBottomView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return onPageChange(item);
            }
        });
        updateUserInfo(navigationView);
        checkPermission();
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0);
            return;
        }
        SongUtils.getSongList(getApplicationContext());
        mDBHelper.insertSong(SongUtils.mSongList);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            SongUtils.getSongList(getApplicationContext());
            mDBHelper.insertSong(SongUtils.mSongList);
        }
    }

    private void updateUserInfo(NavigationView navigationView){
        if (navigationView != null) {
            View header = navigationView.getHeaderView(0);
            TextView txtUsername, txtEmail;
            ImageView imgAvatar;
            txtUsername = header.findViewById(R.id.txt_username);
            txtEmail = header.findViewById(R.id.txt_user_email);
            imgAvatar = header.findViewById(R.id.img_user_avatar);
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null){
                txtUsername.setText(user.getDisplayName());
                txtEmail.setText(user.getEmail());
                new DownloadImageTask(imgAvatar).execute(user.getPhotoUrl().toString()+ "?height=100");
            }
        }
    }

    private boolean onPageChange(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.music:
                handleChangePage(HomePage.MUSIC);
                return true;
            case R.id.video:
                handleChangePage(HomePage.VIDEO);
                return true;
            case R.id.top:
                handleChangePage(HomePage.TOP);
                return true;
            case R.id.news:
                handleChangePage(HomePage.NEWS);
                return true;
            case R.id.profile:
                handleChangePage(HomePage.PROFILE);
                return true;
            default:
                return true;
        }
    }

    private void handleChangePage(@HomePage int position) {
        if (position < 0 || position >= mAdapter.getCount()) {
            return;
        }

        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_music) {
            handleChangePage(HomePage.MUSIC);
        } else if (id == R.id.nav_video) {
            handleChangePage(HomePage.VIDEO);
        } else if (id == R.id.nav_top) {
            handleChangePage(HomePage.TOP);
        } else if (id == R.id.nav_news) {
            handleChangePage(HomePage.NEWS);
        } else if (id == R.id.nav_profile) {
            handleChangePage(HomePage.PROFILE);
        } else if (id == R.id.nav_logout) {
            signOut();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
    private void signOut() {
        mAuth.signOut();
        LoginManager.getInstance().logOut();
        startActivity(new Intent(this, LoginActivity.class));
        HomeActivity.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent musicIntent = new Intent(this, MediaPlayerService.class);
        musicIntent.putExtra("action", MediaPlayerService.ACTION_PAUSE);
        startService(musicIntent);
        SongUtils.mSongList.clear();
    }
}
