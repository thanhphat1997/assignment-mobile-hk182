package vn.edu.hcmut.phatdo.assignment_mobile_182.news;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;
import vn.edu.hcmut.phatdo.assignment_mobile_182.shared.CircleTransform;


// Note that we specify the custom ViewHolder which gives us access to our views
public class NewsAdapter extends
        RecyclerView.Adapter< NewsAdapter.ViewHolder> {

    // Store a member variable for the contacts
    private List<News> items;
    public String pageToken ;
    // Pass in the contact array into the constructor
    public NewsAdapter(List<News> list) {
        items = list;
        pageToken = "";
    }

    public void addMore(List<News> newItems) {
        int last = items.size();
        items.addAll(newItems);
        notifyItemRangeInserted(last,newItems.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.item_news, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        // Get the data model based on position
        final News news = items.get(position);

        // Set item views based on your views and data model
        holder.singerNameTextView.setText(news.singerName);
        holder.textTextView.setText(news.text);
        Picasso.get().load(news.profileImageURL).transform(new CircleTransform() ).into(holder.profileImageView);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm, MMMM dd, yyyy");
        holder.dateTextView.setText(formatter.format(news.date));
        Picasso.get().load(news.previewImageURL).centerCrop().fit().into(holder.previewImage);
        holder.previewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.watchURL));
                view.getContext().startActivity(browserIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public ImageView profileImageView;
        public TextView singerNameTextView;
        public TextView dateTextView;
        public TextView textTextView;
        public ImageView previewImage;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            final View view = itemView;

            profileImageView = view.findViewById(R.id.news_profile_image);
            singerNameTextView =  view.findViewById(R.id.news_singer_name);
            dateTextView =  view.findViewById(R.id.news_date);
            textTextView = view.findViewById(R.id.news_text);
            previewImage= view.findViewById(R.id.news_preview_image);

        }
    }



}
