package vn.edu.hcmut.phatdo.assignment_mobile_182.data;

import android.os.Parcel;
import android.os.Parcelable;

public class PlaylistModel implements Parcelable {
    public long id;
    public String name;

    public PlaylistModel(long id, String name) {
        this.id = id;
        this.name = name;
    }

    protected PlaylistModel(Parcel in) {
        id = in.readLong();
        name = in.readString();
    }

    public static final Creator<PlaylistModel> CREATOR = new Creator<PlaylistModel>() {
        @Override
        public PlaylistModel createFromParcel(Parcel in) {
            return new PlaylistModel(in);
        }

        @Override
        public PlaylistModel[] newArray(int size) {
            return new PlaylistModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
    }
}
