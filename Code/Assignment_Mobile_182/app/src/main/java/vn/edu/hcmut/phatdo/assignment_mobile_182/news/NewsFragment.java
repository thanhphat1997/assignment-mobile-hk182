package vn.edu.hcmut.phatdo.assignment_mobile_182.news;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {


    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_news, container, false);

        final RecyclerView rvNews = v.findViewById(R.id.rv_news);

        final NewsAdapter adapter = new NewsAdapter(new ArrayList<News>());
        rvNews.setAdapter(adapter);
        //add item divider
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(v.getContext(), DividerItemDecoration.VERTICAL);
        rvNews.addItemDecoration(itemDecoration);
        //add layout
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(v.getContext());
        rvNews.setLayoutManager(linearLayoutManager);
        //add endless scroll
        final NestedScrollView mScrollView = v.findViewById(R.id.news_scroll_view);
        mScrollView.getViewTreeObserver().addOnScrollChangedListener(new EndlessNewsScrollingListener(getActivity(),mScrollView,adapter));

        return v;
    }

}
