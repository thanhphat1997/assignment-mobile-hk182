package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({PlayMusicPage.SONG, PlayMusicPage.PLAY})
@Retention(RetentionPolicy.SOURCE)
public @interface PlayMusicPage {
    int SONG = 0;
    int PLAY = 1;
}
