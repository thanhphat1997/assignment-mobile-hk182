package vn.edu.hcmut.phatdo.assignment_mobile_182.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class SongModel implements Parcelable {
    private long id;
    private Uri icon;
    private String name;
    private String singer;
    private String path;

    public SongModel(long id, Uri icon, String name, String singer, String path) {
        this.id = id;
        this.icon = icon;
        this.name = name;
        this.singer = singer;
        this.path = path;
    }

    protected SongModel(Parcel in) {
        id = in.readLong();
        icon = in.readParcelable(Uri.class.getClassLoader());
        name = in.readString();
        singer = in.readString();
        path = in.readString();
    }

    public static final Creator<SongModel> CREATOR = new Creator<SongModel>() {
        @Override
        public SongModel createFromParcel(Parcel in) {
            return new SongModel(in);
        }

        @Override
        public SongModel[] newArray(int size) {
            return new SongModel[size];
        }
    };

    public long getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Uri getIcon() {
        return icon;
    }

    public void setIcon(Uri icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeParcelable(icon, i);
        parcel.writeString(name);
        parcel.writeString(singer);
        parcel.writeString(path);
    }
}
