package vn.edu.hcmut.phatdo.assignment_mobile_182.play.music;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import vn.edu.hcmut.phatdo.assignment_mobile_182.base.AbsPagerAdapter;

public class PlayMusicAdapter extends AbsPagerAdapter {

    private final int PAGE_COUNT = 2;

    public PlayMusicAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case PlayMusicPage.SONG:
                return new SongListFragment();
            case PlayMusicPage.PLAY:
                return new PlayMusicFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
