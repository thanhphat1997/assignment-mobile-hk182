package vn.edu.hcmut.phatdo.assignment_mobile_182.news;

import java.util.List;

public class VideoResponse {
    public List<VideoData> videoDataList;
    public String nextPageToken;

    public VideoResponse(List<VideoData> videoDataList, String nextPageToken) {
        this.videoDataList = videoDataList;
        this.nextPageToken = nextPageToken;
    }
}
