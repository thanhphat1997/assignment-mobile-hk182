package vn.edu.hcmut.phatdo.assignment_mobile_182.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import androidx.recyclerview.selection.ItemDetailsLookup;

public abstract class AbsViewHolder extends RecyclerView.ViewHolder {
    public AbsViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract ItemDetailsLookup.ItemDetails<Long> getItemDetails();
}
