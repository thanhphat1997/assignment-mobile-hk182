package vn.edu.hcmut.phatdo.assignment_mobile_182.play.video;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;

public class VideoRecyclerViewAdapter extends RecyclerView.Adapter<VideoRecyclerViewAdapter.VideoViewHolder> {

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView txtTitle, txtLength;

        public VideoViewHolder(View itemView) {
            super(itemView);
            avatar = (ImageView)itemView.findViewById(R.id.imgVideoAvatar);
            txtLength = (TextView)itemView.findViewById(R.id.txtVideoLength);
            txtTitle = (TextView)itemView.findViewById(R.id.txtVideoName);
        }
    }

    public interface CustomItemClickListener {
        public void onItemClick(View v, int position);
    }

    private List<VideoModel> listVideoModel;
    CustomItemClickListener listener;

    public VideoRecyclerViewAdapter(List<VideoModel> list, CustomItemClickListener listener) {
        this.listVideoModel = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View videoView = inflater.inflate(R.layout.layout_video_item, viewGroup, false);
        final VideoViewHolder viewHolder = new VideoViewHolder(videoView);
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, viewHolder.getPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder videoViewHolder, int i) {
        VideoModel videoModel = listVideoModel.get(i);
        TextView txtTitle = videoViewHolder.txtTitle;
        txtTitle.setText(videoModel.getTitle());
        TextView txtLength = videoViewHolder.txtLength;
        txtLength.setText(videoModel.getLength());
        ImageView frame = videoViewHolder.avatar;
        frame.setImageBitmap(getVideoFrameImage(videoModel));
        frame.setDrawingCacheEnabled(true);
    }

    private Bitmap getVideoFrameImage(VideoModel model){
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(model.getVideoPath());
        return retriever.getFrameAtTime((model.getRealLength()/2)*1000,MediaMetadataRetriever.OPTION_CLOSEST);
    }

    @Override
    public int getItemCount() {
        return listVideoModel.size();
    }
}
