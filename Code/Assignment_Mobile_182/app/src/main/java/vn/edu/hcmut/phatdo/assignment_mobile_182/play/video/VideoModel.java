package vn.edu.hcmut.phatdo.assignment_mobile_182.play.video;

import java.io.Serializable;

public class VideoModel implements Serializable {

    protected String title;
    protected String length;
    protected String videoPath;
    protected int realLength;

    public VideoModel(String title, String length, String videoPath, int realLength){
        this.title = title;
        this.length = length;
        this.videoPath = videoPath;
        this.realLength = realLength;
    }

    public String getTitle() {
        return title;
    }

    public String getLength() {
        return length;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public int getRealLength() {
        return realLength;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public void setRealLength(int realLength) {
        this.realLength = realLength;
    }
}
