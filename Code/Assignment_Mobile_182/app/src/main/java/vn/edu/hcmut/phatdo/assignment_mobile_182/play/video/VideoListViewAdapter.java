package vn.edu.hcmut.phatdo.assignment_mobile_182.play.video;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.edu.hcmut.phatdo.assignment_mobile_182.R;

public class VideoListViewAdapter extends ArrayAdapter<VideoModel> {

    private Context context;
    private int resource;
    private List<VideoModel> listSongModel;

    public VideoListViewAdapter(Context context, int resource, List<VideoModel> list) {
        super(context, resource, list);
        this.context = context;
        this.resource = resource;
        this.listSongModel = list;
    }

    public class ViewHolder {
        ImageView avatar;
        TextView txtTitle, txtLength;

        ViewHolder(View convertView) {
            avatar = (ImageView)convertView.findViewById(R.id.imgVideoAvatar);
            txtLength = (TextView)convertView.findViewById(R.id.txtVideoLength);
            txtTitle = (TextView)convertView.findViewById(R.id.txtVideoName);
        }
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_video_item, parent, false);
            viewHolder = new ViewHolder(convertView);
//            viewHolder.avatar = (ImageView)convertView.findViewById(R.id.imgVideoAvatar);
//            viewHolder.txtLength = (TextView)convertView.findViewById(R.id.txtVideoLength);
//            viewHolder.txtTitle = (TextView)convertView.findViewById(R.id.txtVideoName);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        VideoModel model = listSongModel.get(position);
        viewHolder.txtTitle.setText(model.getTitle());
        viewHolder.txtLength.setText(model.getLength());
        Bitmap image = getVideoFrameImage(model);
        viewHolder.avatar.setImageBitmap(image);
        viewHolder.avatar.setDrawingCacheEnabled(true);
        return convertView;
    }

    private Bitmap getVideoFrameImage(VideoModel model){
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(model.getVideoPath());
        return retriever.getFrameAtTime((model.getRealLength()/2)*1000,MediaMetadataRetriever.OPTION_CLOSEST);
    }
}
