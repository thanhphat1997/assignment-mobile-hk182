package vn.edu.hcmut.phatdo.assignment_mobile_182.data;

public class SongDBModel {
    public long id;
    public long timestamp;
    public long listen;

    public SongDBModel(long id, long timestamp, long listen) {
        this.id = id;
        this.listen = listen;
        this.timestamp = timestamp;
    }
}
