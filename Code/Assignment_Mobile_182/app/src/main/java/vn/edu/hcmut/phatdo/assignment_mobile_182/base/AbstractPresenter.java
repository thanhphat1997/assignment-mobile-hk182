package vn.edu.hcmut.phatdo.assignment_mobile_182.base;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

public abstract class AbstractPresenter<T> implements IPresenter<T> {
    @Nullable
    protected T mView;

    @Override
    public void onAttach(T pView) {
        mView = pView;
    }

    @CallSuper
    @Override
    public void onDetach() {
        mView = null;
    }

    @Nullable
    public T getView() {
        return mView;
    }
}
