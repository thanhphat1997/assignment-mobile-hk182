﻿DANH SÁCH THÀNH VIÊN NHÓM:
1. PHỒNG QUANG TUẤN - 1513865
2. ĐỖ THÀNH PHÁT - 1512400
3. ĐỖ HỮU PHÚC - 1512502
4. NGUYỄN QUỐC BẢO - 1510177
5. TRẦN NHƯ LỰC - 1511918

Link project gitlab: https://gitlab.com/thanhphat1997/assignment-mobile-hk182
Link clip demo: https://youtu.be/5pMbX1OQfgs


Cấu trúc project:
--
 |_ Assets/ : chứa các hình ảnh jpg, jpeg, gif... được sử dụng trong project
 |_ Code/ : chứa source code android của project
 |_ Presentation/ : chứa file thuyết trình (.pptx)
 |_ Report/ : chứa file mô tả cho screen design
 |_ Screen design/ : chứa project screen design
 |_ Screen design (PNG)/ : chứa ảnh screen design export ra từ project screen design
 |_ Use case/ : chứa usecase và đặc tả use case cụ thể.
